= Cloud native development
 Eric D. Schabell @eschabell
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


Cloud native development is an approach to building and running applications to fully use the advantages of the
cloud computing model (i.e., responsive, elastic, and resilient applications). Red Hat empowers organizations to
build and run scalable applications in modern, dynamic environments such as public, private, and hybrid clouds.

Containers and orchestration, DevOps and continuous delivery, microservices and service meshes, and declarative
application programming interfaces (APIs) are key building blocks of cloud-native application development. These
technologies and techniques deliver loosely coupled systems that are resilient, manageable, and observable.
Combined with robust automation, they allow businesses to make high-impact application improvements frequently
and predictably with minimal effort and risk. Additionally, cloud native architecture and technologies provide
the foundation for newer development models such as serverless computing and Functions-as-a-Service.

Use case: Cloud-native development is an approach to building and running applications to fully exploit the 
advantages of the cloud computing model (i.e. responsive, elastic and resilient applications).

Open the diagrams below directly in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams
use the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your
content.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/cloud-native-development.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/cloud-native-development.drawio?inline=false[[Download Diagram]]
--

--
image:logical-diagrams/cloud-native-development-ld.png[350, 300]
image:logical-diagrams/cloud-native-development-details-ld.png[350,300]
image:schematic-diagrams/cloud-native-development-local-containers-runtimes-sd.png[350, 300]
image:schematic-diagrams/cloud-native-development-local-containers-process-sd.png[350, 300]
image:schematic-diagrams/cloud-native-development-remote-containers-runtimes-sd.png[350, 300]
image:schematic-diagrams/cloud-native-development-remote-containers-process-sd.png[350, 300]
image:schematic-diagrams/cloud-native-development-deployment-sd.png[350, 300]
image:schematic-diagrams/cloud-native-development-deployment-enterprise-registry-sd.png[350, 300]
image:schematic-diagrams/cloud-native-development-deployment-with-python.png[350, 300]
image:schematic-diagrams/cloud-native-development-deployment-with-thoth.png[350, 300]
image:detail-diagrams/developer-ide.png[250, 200]
image:detail-diagrams/maven-repo.png[250, 200]
image:detail-diagrams/scm-system.png[250, 200]
image:detail-diagrams/runtimes-frameworks.png[250, 200]
image:detail-diagrams/integration-frameworks.png[250, 200]
image:detail-diagrams/container-tooling.png[250, 200]
image:detail-diagrams/ci-cd-platform.png[250, 200]
image:detail-diagrams/image-registry.png[250, 200]
image:detail-diagrams/registry-management.png[250, 200]
image:detail-diagrams/s2i-workflow.png[250, 200]
image:detail-diagrams/sandbox-registry.png[250, 200]
image:detail-diagrams/enterprise-registry.png[250, 200]
--

